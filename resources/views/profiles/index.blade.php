@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-3 p-5">
            <img src="https://scontent-mad1-1.cdninstagram.com/v/t51.2885-19/s320x320/150144196_413684549725454_5780707478232687550_n.jpg?tp=1&_nc_ht=scontent-mad1-1.cdninstagram.com&_nc_ohc=vAn5-bhBXM0AX8_6VTk&oh=a8d73aa0b1289017aded7eb21063a631&oe=606973C8" style="height: 150px;" class="rounded-circle" alt="">
        </div>
        <div class="col-9 pt-5">
            <div class="d-flex justify-content-between align-items-baseline">
                <h1>{{ $user -> username }}</h1>

                @can ('update', $user->profile)
                    <a href="/p/create">Add New Post</a>
                @endcan
            </div>

            @can ('update', $user->profile)
                <a href="/profile/{{ $user->id }}/edit">Edit Profile</a>
            @endcan
            <div class="d-flex">
            <div class="pr-5"><strong>{{$user->posts->count()}}</strong> posts</div>
            <div class="pr-5"><strong>23k</strong> followers</div>
            <div class="pr-5"><strong>212</strong> following</div>
        </div>
            <div class="pt-4 font-weight-bold">{{$user->profile->title}}</div>
            <div>{{$user->profile->description}}</div>
            <div><a href="#">{{$user->profile->url ?? 'N/A'}}</a></div>
        </div>
    </div>

    <div class="row pt-5">
        @foreach($user->posts as $post)
        <div class="col-4 pb-4">
            <a href="/p/ {{ $post->id }}">
            <img src="/storage/{{ $post->image }}" alt="Post" class="w-100">
            </a>
        </div>
        @endforeach
    </div>
</div>
@endsection
